# README

## Run the app locally
Ensure that Redis is installed: https://redis.io/topics/quickstart

In one tab, run 
```
redis-server
```

In another, run
```
$ foreman start
```
which will deal with running the web server and the workers for the Sidekiq 
background jobs.

## Run the tests

```
$ bundle exec rails test
```
