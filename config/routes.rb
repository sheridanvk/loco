Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => "pages#home"

  get '/listings', to: 'listings#index', as: :listings
  get '/listings/check_listings_loaded', to: 'listings#check_listings_loaded', as: :listings_loaded
  get '/listing/:id', to: 'listings#show', as: :listing
  get '/cities', to: 'cities#index', as: :cities

  resources :artists
end
