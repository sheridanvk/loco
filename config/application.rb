require_relative 'boot'

require 'rails/all'

require 'rake/dsl_definition'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Loco
  class Application < Rails::Application
    config.middleware.use Rack::Deflater
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.spotify_access_token = nil
    config.spotify_expiry_time = nil
  end
end
