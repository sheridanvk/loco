class ListingFetchWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker

  def perform(lat, lng, date)
    ers = ListingRetrievalService.new()

    # have to recast the date to a Date, because it gets converted to a string
    # on its way to Redis
    dateObj = date.to_date
    puts "Date perform: #{dateObj+1}"
    ers.download_listings_from_provider(lat, lng, dateObj)

    total ers.get_total_listings()
    at ers.get_current_listing()
  end
end
