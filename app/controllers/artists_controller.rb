class ArtistsController < ApplicationController
  http_basic_authenticate_with :name => ENV["AUTH_USER"], :password => ENV["AUTH_PASSWORD"]
  def show
    @artist = Artist.find_by(songkick_id: params[:id])
  end

  def edit
    @artist = Artist.find_by(songkick_id: params[:id])
  end

  def update
    @artist = Artist.find(params[:id])

    if @artist.update(artist_params)
      flash[:notice] = 'Successfully updated'
      redirect_to artist_path(@artist.songkick_id)
    else
      flash[:error] = 'There was a problem saving the record'
      redirect_to edit_artist_path(@artist.songkick_id)
    end
  end

  private
  def artist_params
    params.require(:artist).permit(:name, :spotify_artist_id, :is_human_verified)
  end
end
