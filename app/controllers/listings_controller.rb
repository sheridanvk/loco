class ListingsController < ApplicationController
  def index
    lat = params[:lat]
    lng = params[:lng]
    date = params[:date].to_date

    ers = ListingRetrievalService.new()
    @listings = ers.return_stored_listings(lat, lng, date)

    if @listings.length == 0
      more_listings = check_for_listings(lat, lng, date)
      if more_listings
        job_id = ListingFetchWorker.perform_async(lat, lng, date)
        render :json => {
          status: "loading",
          job_id: job_id
        }
      else
        render :json => {status: "no results"}
      end
    else
      render :json => {
        status: "ok",
        listings: @listings.to_json(:include => {lineups: {include: :artist}})
      }
    end
  end

  def show
    id = params[:id]
    @listing = Listing.find(id)
    render :json => {
      listing: @listing.to_json(:include => {lineups: {include: :artist}})
    }
  end

  def check_listings_loaded
    job_id = params[:job_id]

    render :json => {
      status: Sidekiq::Status::status(job_id),
      total: Sidekiq::Status::total(job_id),
      at: Sidekiq::Status::at(job_id)
    }
  end

  private
    def check_for_listings(lat, lng, date)
      songkick_listings = Songkick.fetch_listings(lat, lng, date)
      if songkick_listings.nil?
        return false
      else
        songkick_listings.map do |listing|
          e = Listing.where(songkick_id: listing.fetch(:songkick_id))
          if e[0].nil?
            return true
          end
        end
        return false
      end
    end
end
