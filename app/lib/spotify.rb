require "httparty"

module Spotify

  def self.fetch_artist_details(artist_name)
    # remove anything from the end of the artist_name in parentheses
    artist_name = artist_name.gsub(/[(].*[)]\z/,"").strip

    spotify_artist = fetch_artist(artist_name)
    if spotify_artist
      spotify_artist_id = spotify_artist.fetch(:spotify_artist_id)
      {
        :spotify_artist_id => spotify_artist_id,
        :avatar_url => spotify_artist.fetch(:avatar_url),
        :top_tracks_uri => spotify_artist.fetch(:top_tracks_uri),
      }
    else
      return nil
    end
  end

  def self.fetch_artist(artist_name)
    response = fetch("/search",{
        :q => artist_name,
        :type => "artist",
      })

    begin
      artists = response.fetch("artists").fetch("items")

      if artists.length > 0
        # Find the first artist that is an exact case-insensitive match on the name,
        # or default to the first result
        index = artists.each_index.detect{|i| artists[i].fetch("name").casecmp(artist_name) == 0}

        if (index)
          artist = artists[index]
        else
          artist = artists[0]
        end
      else
        return nil
      end

      spotify_artist_id = artist.fetch("id")
      avatar_url = artist.fetch("images")
      uri = artist.fetch("uri")

      {
        :spotify_artist_id => spotify_artist_id,
        :avatar_url => avatar_url,
        :top_tracks_uri => uri,
      }
    rescue => error
      puts "Error: #{error}"
      puts "Response: #{response}"
      puts error.backtrace.join("\n")
    end
  end

  def self.fetch(path, options)
    access_token = get_access_token()

    begin
      if access_token
        tries ||= 3

        headers = {"Authorization" => "Bearer #{access_token}"}
        response = HTTParty.get("https://api.spotify.com/v1#{path}", headers: headers, query: options)

        if response.code >= 200 && response.code <= 299
          response
        elsif response.code >= 400 && response.code <= 499
          puts "Permanent Error: #{response.code}"
          return nil
        else
          raise
        end
      else
        puts "Unable to get authentication from Spotify, try again later"
        return nil
      end
    rescue  => error
      puts "Error: #{error}"
      puts "Event: #{response}"
      puts error.backtrace.join("\n")
      retry unless (tries -= 1).zero?
    end
  end

  def self.get_access_token
    if Rails.application.config.spotify_access_token && Time.now() < Rails.application.config.spotify_expiry_time
      Rails.application.config.spotify_access_token
    else
      client_id = ENV["SPOTIFY_CLIENT_ID"]
      client_secret = ENV["SPOTIFY_CLIENT_SECRET"]

      headers = {
        "Authorization"  => "Basic "+ Base64.strict_encode64("#{client_id}:#{client_secret}"),
        "Accept" => "application/json",
      }
      body = {
        "grant_type"=>"client_credentials",
      }

      begin
        response = HTTParty.post("https://accounts.spotify.com/api/token", body: body, headers: headers)
      rescue HTTParty::Error => e
        puts 'HttParty::Error '+ e.message
        return
      rescue StandardError => e
        puts 'StandardError '+ e.message
        return
      end

      if response.parsed_response
        token = response.parsed_response

        Rails.application.config.spotify_access_token = token.fetch("access_token")
        Rails.application.config.spotify_expiry_time = Time.now()- 60 + token.fetch("expires_in")

        Rails.application.config.spotify_access_token
      else
        puts "Failed to get Spotify authentication token"
      end
    end
  end
end
