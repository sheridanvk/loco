class City < ApplicationRecord
  validates_uniqueness_of :lat, :scope => [:lng]
end
