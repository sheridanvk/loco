class Listing < ApplicationRecord
  has_many :lineups,  :dependent => :destroy
  has_many :artists, through: :lineups

  validates :songkick_id, uniqueness: true
  validates :start_date, presence: true

  after_save :generate_geometry_point, unless: :geometry?

  def start_time=(new_start_time)
    # When saving the start time of an listing, we also need to know what time it
    # is locally, in order to display this to the user wherever they are in the
    # world.
    if (!new_start_time.nil?)
      start_time_local = new_start_time + new_start_time.utc_offset
      write_attribute(:start_time, new_start_time)
      write_attribute(:start_time_local, start_time_local)
    end
  end

  def self.nearby(lat, lng, distance: 10000)
    where("ST_DWithin(listings.geometry, ST_GeographyFromText('SRID=4326;POINT(#{lng} #{lat})'), #{distance})")
  end

  private
  def generate_geometry_point
    if (self.lng && self.lat)
      self.geometry = "POINT(#{self.lng} #{self.lat})"
      self.save
    end
  end

  def start_time_local=(val)
    write_attribute :start_time_local, val
  end
end
