class ListingRetrievalService
  def set_total_listings(n)
    @total_listings = n
  end

  def get_total_listings
    @total_listings
  end

  def set_current_listing(n)
    @current_listing = n
  end

  def get_current_listing
    @current_listing
  end

  def return_stored_listings(lat, lng, date)
      # Either we match to local time if given, or we look at the date
      # We also pull the next day's listings, to buffer for timezone issues
      Listing.nearby(lat, lng)
      .where("(start_time_local < ? and start_time_local >= ?) OR start_date = ?", date + 1, date, date)
  end

  def download_listings_from_provider(lat, lng, date)
    @listings = Songkick.fetch_listings(lat,lng, date)
    self.set_total_listings(@listings.count)
    self.set_current_listing(0)

    if @listings
      @listings.map do |listing|
        begin
          self.set_current_listing(self.get_current_listing + 1)
          e = Listing.create(
            start_time: listing.fetch(:start_time),
            start_date: listing.fetch(:start_date),
            listing_url: listing.fetch(:listing_url),
            venue_name: listing.fetch(:location).fetch(:venue_name),
            lat: listing.fetch(:location).fetch(:lat),
            lng: listing.fetch(:location).fetch(:lng),
            songkick_id: listing.fetch(:songkick_id)
          )

          artists = listing.fetch(:artists)
          if artists.length > 0
            artists.map do |artist|
              a = Artist.find_or_create_by(
                name: artist.fetch(:name),
                songkick_id: artist.fetch(:songkick_id)
              )
              l = Lineup.create(
                listing_id: e.id,
                artist_id: a.id,
                is_headline: artist.fetch(:is_headline)
              )
            end
          else
            raise
          end
        rescue => error
          puts "Error: #{error}"
          puts "Listing: #{listing}"
          puts error.backtrace.join("\n")
          if e
            e.destroy()
          end
          next
        end
      end
    end
  end
end
