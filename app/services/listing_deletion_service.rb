class ListingDeletionService
  def remove_old_listings()
    Listing.where("start_date < ?", Date.today).destroy_all
  end
end
