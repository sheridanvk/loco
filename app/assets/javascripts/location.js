const checkLocationPermissions = () => {
  const locReqEl = document.getElementById('location-request')
  if(Cookies.get("location-permission") == "rejected") {
    loadDefaultLocation();
  }
  else if (navigator.permissions) {
    navigator.permissions.query({'name': 'geolocation'})
    .then((permission) => {
       if (permission.state == "prompt") {
         // display window asking for location permission
         locReqEl.style.display = 'flex';
       }
       else if (permission.state == "denied") {
         loadDefaultLocation();
       }
       else if (permission.state == "granted") {
         getLocation();
       }
     }
    )
  }
  else if(Cookies.get("location-permission") == "granted") {
    getLocation();
  }
  else {
    locReqEl.style.display = 'flex';
  }

}

const moveToLocation = (lat, lng) => {
  const today = new Date();
  getListingsForLocation(lat, lng, today);
  mapEl.jumpTo({
    center: [
      lng,
      lat
    ]
  });
}

const getLocation = () => {
  if (navigator.geolocation) {
    const success = (position) => {
        moveToLocation(position.coords.latitude, position.coords.longitude);
    };
    const error = (err) => {
      loadDefaultLocation();
      //TODO: Add something saying we can't get your location, check location settings
    };
    const options =    {
    //     timeout: 5000
    }
    navigator.geolocation.getCurrentPosition(success, error, options);
  }
  // TODO: Add something that wipes the "granted" state of the cookie if this
  // fails and returns default location
};

const setLocationCookie = (state) => {
  const locReqEl = document.getElementById('location-request')
  if(!(locReqEl.style.display === 'none')) {
    locReqEl.style.display = 'none'
  }
  Cookies.set("location-permission", state);

  if (state == "granted")
    getLocation();
  else
    loadDefaultLocation();
}

const loadDefaultLocation = () => {
  // return listings for a default location, Berlin
  const today = new Date();
  getListingsForLocation(52.5200, 13.4050, today);
}

const showLoadingScreen = (jobId, lat, lng, date) => {
  const loadingEl = document.getElementById('loading')
  loadingEl.style.display = 'flex';
  const checkListingsLoaded = setInterval(() => {
    fetch(`${Routes.listings_loaded_path()}?job_id=${jobId}`, {method: 'get'})
    .then((response) => response.json())
    .then((data) => {
      console.log(`Status: ${data.status}, Total: ${data.total}, At: ${data.at}`)

      if (data.status == "complete") {
        clearInterval(checkListingsLoaded);
        if(!loadingEl.hidden) {
          loadingEl.style.display = 'none';
        };
        getListingsForLocation(lat, lng, date)
      }
    });
  }, 1000);
}
