class Carousel extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      artistIndex: 0,
      numArtists: this.props.listing.lineups.length,
      currArtist: this.props.listing.lineups[0].artist
    }
  }

  getTimeString(start_time) {
    let timeString = '';
    if (start_time) {
      const datetimeObject = new Date(start_time);
      timeString = datetimeObject.toLocaleTimeString([], {
        timeZone: 'UTC',
        hour: "2-digit",
        minute: "2-digit"
      })
    } else {
      timeString = "Check listing"
    }
    return timeString;
  }

  changeCarouselPosition(direction) {
    if (this.getCarouselState(direction) === 'active') {
      let indexChange = 0;
      if (direction == 'forward') {
        indexChange = 1;
      }
      else {
        indexChange = -1;
      }
      this.state.artistIndex += indexChange;
      this.setState({currArtist: this.props.listing.lineups[this.state.artistIndex].artist});
    }
  }

  getCarouselState(direction) {
    if (this.state.numArtists > 1) {
      if (direction === 'forward') {
        if (this.state.artistIndex === (this.state.numArtists - 1)) {
          return '';
        } else return 'active';
      } else {
        if (this.state.artistIndex === 0) {
          return '';
        } else return 'active';
      }
    } else {
      return 'hidden'
    }
  }

  getPlayerHTML () {
    const spotifyId = this.state.currArtist.spotify_artist_id

    if (spotifyId) {
      return <div className="iframe-wrapper">
        <div className="iframe-loading"><p>Loading music...</p></div>
        <iframe key={spotifyId} src={"https://open.spotify.com/embed?uri=spotify:artist:" + spotifyId} width="250" height="80" frameBorder="0" style={{
          gridArea: "1 / 1 / 1 / 1"}} allowtransparency="true" allow="encrypted-media"/>
      </div>
    } else {
      return <p className="no-music">Can't find music on Spotify :( try <a href={"https://www.google.com/search?q='" + encodeURI(this.state.currArtist.name) + "'+band"}
      target="_blank">Google?</a></p>
    }
  }

  render() {
    return <div>
      <div className="row">
        <div className="time">
          <p>{this.getTimeString(this.props.listing.start_time_local)}</p>
        </div>
        <div className="carousel row">
          <div id="back" className={"carousel-button " + this.getCarouselState('back')} onClick={() => this.changeCarouselPosition('back')}></div>
          <div id="forward" className={"carousel-button " + this.getCarouselState('forward')} onClick={() => this.changeCarouselPosition('forward')}></div>
        </div>
      </div>
      <div className="row">
          <div className="artist">
            <p className="artist-name">{this.state.currArtist.name}</p>
            <div className="centeredRow">
                <div className="musicPlayer">
                  {this.getPlayerHTML()}
                </div>
            </div>
          </div>
      </div>
      <div className="row">
        <div className="venue">
          <p>{this.props.listing.venue_name}</p>
        </div>
      </div>
      <div className="row">
        <div className="attribution">
          <a href={this.props.listing.listing_url} target="_blank">Tickets and more by Songkick</a>
        </div>
      </div>
  </div>
  }
}
