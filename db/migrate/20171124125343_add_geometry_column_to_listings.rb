class AddGeometryColumnToListings < ActiveRecord::Migration[5.1]
  def up
    enable_extension :postgis
    add_column :listings, :geometry, :st_point, geographic: true
    add_index :listings, :geometry, using: :gist
  end

  def down
    remove_column :listings, :geometry, :st_point, geographic: true
  end
end
