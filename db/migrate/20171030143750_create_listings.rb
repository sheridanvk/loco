class CreateListings < ActiveRecord::Migration[5.1]
  def change
    create_table :listings do |t|
      t.datetime :start_time
      t.string :listing_url
      t.string :venue_name
      t.float :lat
      t.float :lng

      t.timestamps
    end
  end
end
