class ChangeStartTimeFormatInListings < ActiveRecord::Migration[5.1]
  def up
    remove_column :listings, :start_time, :time
    add_column :listings, :start_time, :datetime
  end

  def down
    remove_column :listings, :start_time, :datetime
    add_column :listings, :start_time, :time
  end
end
