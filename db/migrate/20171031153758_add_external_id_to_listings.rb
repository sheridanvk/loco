class AddExternalIdToListings < ActiveRecord::Migration[5.1]
  def change
    add_column :listings, :songkick_id, :string
  end
end
