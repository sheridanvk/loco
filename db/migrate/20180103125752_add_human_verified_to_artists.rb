class AddHumanVerifiedToArtists < ActiveRecord::Migration[5.1]
  def change
    add_column :artists, :human_verified?, :boolean
  end
end
