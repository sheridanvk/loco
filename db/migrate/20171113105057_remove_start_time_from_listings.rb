class RemoveStartTimeFromListings < ActiveRecord::Migration[5.1]
  def change
    remove_column :listings, :start_time, :datetime
  end
end
