class AddIndexToSongkickIdArtists < ActiveRecord::Migration[5.1]
  def change
    add_index :artists, :songkick_id, unique: true
  end
end
