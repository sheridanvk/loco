class RenameHumanVerifiedArtists < ActiveRecord::Migration[5.1]
  def change
    rename_column :artists, :human_verified?, :is_human_verified
  end
end
