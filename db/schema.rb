# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180126120429) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "artists", force: :cascade do |t|
    t.string "name"
    t.string "spotify_artist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "songkick_id"
    t.boolean "is_human_verified"
    t.index ["songkick_id"], name: "index_artists_on_songkick_id", unique: true
  end

  create_table "cities", force: :cascade do |t|
    t.string "name"
    t.float "lat"
    t.float "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lineups", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "listing_id"
    t.integer "artist_id"
    t.boolean "is_headline"
  end

  create_table "listings", force: :cascade do |t|
    t.string "listing_url"
    t.string "venue_name"
    t.float "lat"
    t.float "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "songkick_id"
    t.date "start_date"
    t.datetime "start_time"
    t.geography "geometry", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.datetime "start_time_local"
    t.index ["geometry"], name: "index_listings_on_geometry", using: :gist
  end

end
