require 'test_helper'
require 'minitest/mock'

class ListingRetrievalServiceTest < ActiveSupport::TestCase
  test 'saving listings, lineups and artists' do
    response = JSON.parse(File.read('test/fixtures/files/listings.json'))
    date = '2017-10-15'.to_date

    Songkick.stub(:fetch, response) do
      ListingRetrievalService.new().download_listings_from_provider(52.48465, 13.39221, date)
      listings = Listing.all

      assert_equal(listings[0].lineups[0].artist.name, "Weezer")
      assert_equal(listings[1].lineups[0].artist.name, "Lorde")
      assert_equal(listings[1].lineups[1].artist.name, "Khalid")
      assert_equal(listings[2].lineups[0].artist.name,"Streichquartett")
    end
  end

  test "don't save malformed listing" do
    # This listing is missing artist information, so make sure it doesn't get
    # saved to the DB
    response = JSON.parse(File.read('test/fixtures/files/malformed_listing.json'))
    date = '2017-10-15'.to_date

    Songkick.stub(:fetch, response) do
      ListingRetrievalService.new().download_listings_from_provider(52.48465, 13.39221, date)
      listing = Listing.where(songkick_id: 32065109)
      assert_nil(listing[0])
    end
  end
end
