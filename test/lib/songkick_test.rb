require 'test_helper'
require 'minitest/mock'

class SongkickTest < ActiveSupport::TestCase
  test 'fetching some listings' do
    response = JSON.parse(File.read('test/fixtures/files/listings.json'))
    date = '2017-10-15'.to_date

    Songkick.stub(:fetch, response) do
      listings = Songkick.fetch_listings(52.48465, 13.39221, date)

      assert_equal(
        listings,
        [{
          :location => {
            :venue_name => 'Columbiahalle',
            :lat => 52.48465,
            :lng => 13.39221
          },
          :artists => [
             {
               :name => 'Weezer',
               :songkick_id => 544909,
               :is_headline => true
             }
          ],
          :start_time => Time.parse('2017-10-15T20:00:00+0200'),
          :start_date => Date.parse('2017-10-15'),
          :listing_url => 'http://www.songkick.com/concerts/29619944-weezer-at-columbiahalle?utm_source=44707&utm_medium=partner',
          :songkick_id => 29619944
        }, {
          :location => {
            :venue_name => 'Tempodrome',
            :lat => 52.501626,
            :lng => 13.381228
          },
          :artists => [
            {
              :name => 'Lorde',
              :songkick_id => 6715369,
              :is_headline => true
            }, {
              :name => 'Khalid',
              :songkick_id => 894756,
              :is_headline => false
            }
          ],
          :start_time => nil,
          :start_date => Date.parse('2017-10-15'),
          :listing_url => 'http://www.songkick.com/concerts/30406814-lorde-at-tempodrome?utm_source=44707&utm_medium=partner',
          :songkick_id => 30406814
        }, {
          :artists => [
            {
              :name => "Streichquartett",
              :songkick_id => 5561998,
              :is_headline => true
              }
            ],
            :location => {
              :venue_name=>"Radialsystem V - Halle",
              :lat=>52.51057,
              :lng=>13.42873
            },
            :start_time => nil,
            :start_date => Date.parse('2017-12-01'),
            :listing_url => "http://www.songkick.com/concerts/31929574-streichquartett-at-radialsystem-v-halle?utm_source=44707&utm_medium=partner",
            :songkick_id=>31929574
          }
        ]
      )
    end
  end
end
